import React, { useState, useRef, useEffect } from 'react';
import { Header } from './Header';
import { Form } from './Form';
import { List } from './List';

export const App = () => {
  const [todos, setTodos] = useState([]);

  const formRef = useRef(null);

  useEffect(() => {
    formRef.current && formRef.current.focus();
  }, []);

  const addTodo = todo => {
    setTodos(prev => {
      return [todo, ...prev];
    });
  };

  const removeTodo = id => {
    setTodos(prev => {
      return prev.filter(todo => {
        return todo.id !== id;
      });
    });
  };

  const setCompleted = (id, completed) => {
    setTodos(prev => {
      const updated = prev.map(todo => {
        if (todo.id === id) {
          return { ...todo, completed };
        }
        return todo;
      });

      return updated;
    });
  };

  return (
    <div>
      <Header text="Список дел" />
      <Form ref={formRef} onAddTodo={addTodo} />
      <List
        todos={todos}
        onSetCompleted={setCompleted}
        onRemoveTodo={removeTodo}
      />
    </div>
  );
};

export default App;
