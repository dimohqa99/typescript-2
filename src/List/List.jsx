import React from 'react';
import { Todo } from '../Todo';

export const List = ({ todos, onSetCompleted, onRemoveTodo }) => {
  return (
    <div>
      {todos.map(todo => (
        <Todo
          key={todo.id}
          {...todo}
          onCheck={() => {
            onSetCompleted(todo.id, !todo.completed);
          }}
          onRemove={() => {
            onRemoveTodo(todo.id);
          }}
        >
          {todo.text}
        </Todo>
      ))}
    </div>
  );
};
