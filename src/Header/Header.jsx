import React from 'react';

import { makeClasses } from './styles';

export const Header = ({ text }) => (
  <div className={makeClasses().root}>
    <h1 className={makeClasses().text}>{text}</h1>
  </div>
);
