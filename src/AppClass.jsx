import React from 'react';
import { Header } from './Header';
import { Form } from './Form';
import { List } from './List';

export class AppClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
    };

    this.formRef = React.createRef(null);
  }

  componentDidMount() {
    this.formRef.current && this.formRef.current.focus();
  }

  addTodo(todo) {
    this.setState({ todos: [todo, ...this.state.todos] });
  }

  removeTodo(id) {
    this.setState({
      todos: this.state.todos.filter(todo => {
        return todo.id !== id;
      }),
    });
  }

  setCompleted(id, completed) {
    const updated = this.state.todos.map(todo => {
      if (todo.id === id) {
        return { ...todo, completed };
      }
      return todo;
    });

    this.setState({ todos: updated });
  }

  render() {
    return (
      <div>
        <Header text="Список дел" />
        <Form ref={this.formRef} onAddTodo={todo => this.addTodo(todo)} />
        <List
          todos={this.state.todos}
          onSetCompleted={(id, completed) => {
            this.setCompleted(id, completed);
          }}
          onRemoveTodo={id => {
            this.removeTodo(id);
          }}
        />
      </div>
    );
  }
}
